﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quit : MonoBehaviour
{

    private Button button;
    private void Start()
    {
        button = this.GetComponent<Button>();
        button.onClick.AddListener(ExitNow);
    }

    public void ExitNow()
    {
        Debug.Log("exit");
        Application.Quit();
    }

}