﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class StartGame : MonoBehaviour
{

    public void LoadALevel(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }
}

