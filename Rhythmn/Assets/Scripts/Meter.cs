﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meter : MonoBehaviour {

	int multiplier=2;
	int streak=0;

	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt("Score",0);
		PlayerPrefs.SetInt("Meter",25);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTrifferEnter2D(Collider2D col){

	}

	public void AddStreak(){
        if (PlayerPrefs.GetInt("Meter") + 1 < 5){
            PlayerPrefs.SetInt("Meter", PlayerPrefs.GetInt("Meter") + 1);
        }
		streak++;
		if (streak >= 24)
			multiplier = 4;
		else if (streak > 16)
			multiplier = 3;
		else if (streak >= 8)
			multiplier = 2;
		else
			multiplier = 1;
		UpdateGUI ();

	}

	public void ResetStreak(){
        if(PlayerPrefs.GetInt("Meter") > 0){
            PlayerPrefs.SetInt("Meter", PlayerPrefs.GetInt("Meter") - 2);
        }
		streak=0;
		multiplier=1;
		UpdateGUI();

	}

	void UpdateGUI(){
		PlayerPrefs.SetInt ("Streak", streak);
		PlayerPrefs.SetInt ("Mult", multiplier);

	}

    public int GetScore()
    {
        return 100 * multiplier;
    }
}
